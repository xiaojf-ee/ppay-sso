package cc.ppay.sso.server.service;

import cc.ppay.sso.common.model.CommonResult;
import cc.ppay.sso.server.model.UserInfo;

/**
 * 用户服务
 *
 * @author Ade.Xiao 2021/1/29 20:47
 */
public interface UserService {
    /**
     * 用户身份校验
     *
     * @param username
     * @param password
     * @return
     * @author Ade.Xiao 2021/1/29 20:47
     */
    CommonResult<UserInfo> findUser(String username, String password);
}
