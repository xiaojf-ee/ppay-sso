package cc.ppay.sso.server.web.controller;

import cc.ppay.sso.common.constant.SsoConstant;
import cc.ppay.sso.common.model.CommonResult;
import cc.ppay.sso.common.model.SsoUser;
import cc.ppay.sso.common.util.RequestUtil;
import cc.ppay.sso.core.login.SsoWebLoginHelper;
import cc.ppay.sso.core.store.SsoLoginStore;
import cc.ppay.sso.core.store.SsoSessionIdHelper;
import cc.ppay.sso.server.model.UserInfo;
import cc.ppay.sso.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.UUID;

/**
 * sso server (for web)
 *
 * @author xuxueli 2017-08-01 21:39:47
 */
@Controller
public class WebController {

    @Autowired
    private UserService userService;
    @Autowired
    private SsoWebLoginHelper ssoWebLoginHelper;
    @Autowired
    private SsoLoginStore ssoLoginStore;
    @Autowired
    private SsoSessionIdHelper ssoSessionIdHelper;

    @RequestMapping("/")
    public String index(Model model, HttpServletRequest request, HttpServletResponse response) {

        // login check
        SsoUser ssoUser = ssoWebLoginHelper.loginCheck(request, response);

        if (ssoUser == null) {
            return "redirect:/login";
        } else {
            model.addAttribute("ssoUser", ssoUser);
            return "index";
        }
    }

    /**
     * Login page
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(SsoConstant.SSO_LOGIN)
    public String login(Model model, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {

        // login check
        SsoUser ssoUser = ssoWebLoginHelper.loginCheck(request, response);

        if (ssoUser != null) {

            // success redirect
            String redirectUrl = request.getParameter(SsoConstant.REDIRECT_URL);
            if (redirectUrl != null && redirectUrl.trim().length() > 0) {

                String sessionId = ssoWebLoginHelper.getSessionIdByCookie(request);
                String redirectUrlFinal = redirectUrl + "?" + SsoConstant.SSO_SESSIONID + "=" + URLEncoder.encode(sessionId, "UTF-8");

                return "redirect:" + redirectUrlFinal;
            } else {
                return "redirect:/";
            }
        }

        model.addAttribute("errorMsg", request.getParameter("errorMsg"));
        model.addAttribute(SsoConstant.REDIRECT_URL, request.getParameter(SsoConstant.REDIRECT_URL));
        return "login";
    }

    /**
     * Login
     *
     * @param request
     * @param redirectAttributes
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("/doLogin")
    public String doLogin(HttpServletRequest request,
                          HttpServletResponse response,
                          RedirectAttributes redirectAttributes,
                          String username,
                          String password,
                          String ifRemember) {

        boolean ifRem = (ifRemember != null && "on".equals(ifRemember)) ? true : false;

        // valid login
        CommonResult<UserInfo> result = userService.findUser(username, password);
        if (result.getCode() != CommonResult.SUCCESS_CODE) {
            redirectAttributes.addAttribute("errorMsg", result.getMsg());

            redirectAttributes.addAttribute(SsoConstant.REDIRECT_URL, request.getParameter(SsoConstant.REDIRECT_URL));
            return "redirect:/login";
        }

        SsoUser ssoUser = new SsoUser();
        ssoUser.setUserid(String.valueOf(result.getData().getUserid()));
        ssoUser.setUsername(result.getData().getUsername());
        ssoUser.setVersion(UUID.randomUUID().toString().replaceAll("-", ""));
        ssoUser.setExpireMinute(ssoLoginStore.getRedisExpireMinute());
        ssoUser.setExpireFreshTime(System.currentTimeMillis());

        // 2、make session id
        String sessionId = ssoSessionIdHelper.makeSessionId(SsoConstant.AUTH_TYPE_WEB, RequestUtil.getIp(request), ssoUser);

        // 3、login, store storeKey + cookie sessionId
        ssoWebLoginHelper.login(response, sessionId, ssoUser, ifRem);

        // 4、return, redirect sessionId
        String redirectUrl = request.getParameter(SsoConstant.REDIRECT_URL);
        if (redirectUrl != null && redirectUrl.trim().length() > 0) {
            String redirectUrlFinal = redirectUrl + "?" + SsoConstant.SSO_SESSIONID + "=" + sessionId;
            return "redirect:" + redirectUrlFinal;
        } else {
            return "redirect:/";
        }

    }

    /**
     * Logout
     *
     * @param request
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(SsoConstant.SSO_LOGOUT)
    public String logout(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {

        // logout
        ssoWebLoginHelper.logout(request, response);

        redirectAttributes.addAttribute(SsoConstant.REDIRECT_URL, request.getParameter(SsoConstant.REDIRECT_URL));
        return "redirect:/login";
    }


}
