package cc.ppay.sso.server.service.impl;

import cc.ppay.sso.common.model.CommonResult;
import cc.ppay.sso.server.model.UserInfo;
import cc.ppay.sso.server.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户服务
 *
 * @author Ade.Xiao 2021/1/29 20:47
 */
@Service
public class UserServiceImpl implements UserService {

    private static List<UserInfo> mockUserList = new ArrayList<>();

    static {
        for (int i = 0; i < 5; i++) {
            UserInfo userInfo = new UserInfo();
            userInfo.setUserid(1000 + i);
            userInfo.setUsername("user" + (i > 0 ? String.valueOf(i) : ""));
            userInfo.setPassword("123456");
            mockUserList.add(userInfo);
        }
    }

    @Override
    public CommonResult<UserInfo> findUser(String username, String password) {

        if (username == null || username.trim().length() == 0) {
            return new CommonResult<>(CommonResult.FAIL_CODE, "Please input username.");
        }
        if (password == null || password.trim().length() == 0) {
            return new CommonResult<>(CommonResult.FAIL_CODE, "Please input password.");
        }

        // mock user
        for (UserInfo mockUser : mockUserList) {
            if (mockUser.getUsername().equals(username) && mockUser.getPassword().equals(password)) {
                return new CommonResult<>(mockUser);
            }
        }

        return new CommonResult<>(CommonResult.FAIL_CODE, "username or password is invalid.");
    }


}
