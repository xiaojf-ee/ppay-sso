package cc.ppay.sso.server.web.controller;

import cc.ppay.sso.common.model.CommonResult;
import cc.ppay.sso.common.model.SsoUser;
import cc.ppay.sso.core.login.SsoTokenLoginHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 业务
 *
 * @author Ade.Xiao 2021/1/30 14:27
 */
@RestController
@RequestMapping("/biz")
public class BizController {
    @Autowired
    private SsoTokenLoginHelper ssoTokenLoginHelper;

    @RequestMapping("/logout")
    public CommonResult logout(String sessionid) {
        ssoTokenLoginHelper.logout(sessionid);
        return CommonResult.SUCCESS;
    }

    @RequestMapping("/loginCheck")
    public CommonResult loginCheck(String authType, String sessionid) {
        SsoUser ssoUser = ssoTokenLoginHelper.loginCheck(authType, sessionid);
        if (ssoUser != null) {
            return new CommonResult(ssoUser);
        }

        return CommonResult.FAIL;
    }
}
