package cc.ppay.sso.server.web.controller;

import cc.ppay.sso.common.constant.SsoConstant;
import cc.ppay.sso.common.model.CommonResult;
import cc.ppay.sso.common.model.SsoUser;
import cc.ppay.sso.common.util.RequestUtil;
import cc.ppay.sso.core.login.SsoTokenLoginHelper;
import cc.ppay.sso.core.store.SsoLoginStore;
import cc.ppay.sso.core.store.SsoSessionIdHelper;
import cc.ppay.sso.server.model.UserInfo;
import cc.ppay.sso.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * sso server (for app)
 *
 * @author xuxueli 2018-04-08 21:02:54
 */
@Controller
@RequestMapping("/app")
public class AppController {

    @Autowired
    private UserService userService;
    @Autowired
    private SsoTokenLoginHelper ssoTokenLoginHelper;
    @Autowired
    private SsoLoginStore ssoLoginStore;
    @Autowired
    private SsoSessionIdHelper ssoSessionIdHelper;


    /**
     * Login
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("/login")
    @ResponseBody
    public CommonResult<String> login(String username, String password, HttpServletRequest request) {

        // valid login
        CommonResult<UserInfo> result = userService.findUser(username, password);
        if (result.getCode() != CommonResult.SUCCESS_CODE) {
            return new CommonResult<String>(result.getCode(), result.getMsg());
        }

        // 1、make sso user
        SsoUser ssoUser = new SsoUser();
        ssoUser.setUserid(String.valueOf(result.getData().getUserid()));
        ssoUser.setUsername(result.getData().getUsername());
        ssoUser.setVersion(UUID.randomUUID().toString().replaceAll("-", ""));
        ssoUser.setExpireMinute(ssoLoginStore.getRedisExpireMinute());
        ssoUser.setExpireFreshTime(System.currentTimeMillis());

        // 2、generate sessionId + storeKey
        String sessionId = ssoSessionIdHelper.makeSessionId(SsoConstant.AUTH_TYPE_APP, RequestUtil.getIp(request), ssoUser);

        // 3、login, store storeKey
        ssoTokenLoginHelper.login(sessionId, ssoUser);

        // 4、return sessionId
        return new CommonResult<String>(sessionId);
    }


    /**
     * Logout
     *
     * @param sessionId
     * @return
     */
    @RequestMapping("/logout")
    @ResponseBody
    public CommonResult<String> logout(String sessionId) {
        // logout, remove storeKey
        ssoTokenLoginHelper.logout(sessionId);
        return CommonResult.SUCCESS;
    }

    /**
     * logincheck
     *
     * @param sessionId
     * @return
     */
    @RequestMapping("/logincheck")
    @ResponseBody
    public CommonResult<SsoUser> logincheck(String sessionId) {

        // logout
        SsoUser ssoUser = ssoTokenLoginHelper.loginCheck(SsoConstant.AUTH_TYPE_APP, sessionId);
        if (ssoUser == null) {
            return new CommonResult<SsoUser>(CommonResult.FAIL_CODE, "sso not login.");
        }
        return new CommonResult<SsoUser>(ssoUser);
    }

}
