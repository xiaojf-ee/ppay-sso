package cc.ppay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xuxueli 2018-03-22 23:41:47
 */
@SpringBootApplication
public class PpaySsoServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PpaySsoServerApplication.class, args);
    }

}