<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>统一认证中心</title>
    <link rel="stylesheet" type="text/css" href="${request.contextPath}/static/css/normalize.css"/>
    <link rel="stylesheet" type="text/css" href="${request.contextPath}/static/css/demo.css"/>
    <!--必要样式-->
    <link rel="stylesheet" type="text/css" href="${request.contextPath}/static/css/component.css"/>
    <!--[if IE]>
    <script src="js/html5.js"></script>
    <![endif]-->
</head>
<body>
<div class="container demo-1">
    <div class="content">
        <div id="large-header" class="large-header">
            <canvas id="demo-canvas"></canvas>
            <div class="logo_box">
                <h3>统一认证中心</h3>
                <form action="${request.contextPath}/doLogin" id="loginForm" method="post">
                    <div class="input_outer">
                        <span class="u_user"></span>
                        <input name="username" class="text" style="color: #FFFFFF !important" type="text"
                               placeholder="请输入账户" value="user">
                    </div>
                    <div class="input_outer">
                        <span class="us_uer"></span>
                        <input name="password" class="text"
                               style="color: #FFFFFF !important; position:absolute; z-index:100;" value="123456"
                               type="password" placeholder="请输入密码">
                    </div>

                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="ifRemember"> &nbsp; 记住密码
                        </label>
                    </div>

                    <input type="hidden" name="redirect_url" value="${redirect_url!''}"/>
                    <div class="mb2">
                        <a class="act-but submit" onclick="document:loginForm.submit()" style="color: #FFFFFF">登录</a>
                    </div>

                    <div>
                        <#if errorMsg?exists>
                            <p style="color: red;">${errorMsg}</p>
                        </#if>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="${request.contextPath}/static/js/TweenLite.min.js"></script>
<script src="${request.contextPath}/static/js/EasePack.min.js"></script>
<script src="${request.contextPath}/static/js/rAF.js"></script>
<script src="${request.contextPath}/static/js/demo-1.js"></script>
</body>
</html>