<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>统一认证中心</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
            <a href="${request.contextPath}/logout">
                <span class="hidden-xs">注销【${ssoUser.username}】</span>
            </a>
        </li>
    </ul>
</div>
</body>
</html>
