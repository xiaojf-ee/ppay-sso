package cc.ppay.sso.common.model;

import java.io.Serializable;

/**
 * sso user
 *
 * @author Ade.Xiao 2021/1/29 15:11
 */
public class SsoUser implements Serializable {
    private static final long serialVersionUID = -357977261796943279L;
    /**
     * 用户id
     */
    private String userid;
    /**
     * 用户名
     */
    private String username;
    /**
     * 版本号
     */
    private String version;
    /**
     * 登录失效时间
     */
    private int expireMinute;
    /**
     * 登录更新时间
     */
    private long expireFreshTime;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getExpireMinute() {
        return expireMinute;
    }

    public void setExpireMinute(int expireMinute) {
        this.expireMinute = expireMinute;
    }

    public long getExpireFreshTime() {
        return expireFreshTime;
    }

    public void setExpireFreshTime(long expireFreshTime) {
        this.expireFreshTime = expireFreshTime;
    }
}
