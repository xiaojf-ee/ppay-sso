package cc.ppay.sso.common.model;

import java.io.Serializable;

/**
 * 公共结果
 *
 * @author Ade.Xiao 2020/8/23 8:15
 */
public class CommonResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final int SUCCESS_CODE = 200;
    public static final int FAIL_CODE = 500;

    public static final CommonResult<String> SUCCESS = new CommonResult<String>(200, null);
    public static final CommonResult<String> FAIL = new CommonResult<String>(FAIL_CODE, null);

    public CommonResult() {
    }

    public CommonResult(Boolean success, int code, String msg, T data, long timestamp, String requestId) {
        this.success = success;
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.timestamp = timestamp;
        this.requestId = requestId;
    }

    public CommonResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public CommonResult(T data) {
        this.data = data;
    }

    /**
     * 操作是否成功
     */
    private Boolean success = true;

    /**
     * 编码：200表示成功，其他值表示失败
     */
    private int code = SUCCESS_CODE;
    /**
     * 消息内容
     */
    private String msg;
    /**
     * 响应数据
     */
    private T data;

    /**
     * 时间戳
     */
    private long timestamp;

    /**
     * 请求id
     */
    private String requestId;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
