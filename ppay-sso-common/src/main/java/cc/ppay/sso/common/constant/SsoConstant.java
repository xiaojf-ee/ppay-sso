package cc.ppay.sso.common.constant;

import cc.ppay.sso.common.model.CommonResult;

/**
 * 常量
 *
 * @author Ade.Xiao 2021/1/29 19:52
 */
public class SsoConstant {
    /**
     * sso sessionid, between browser and sso-server (web + token client)
     */
    public static final String SSO_SESSIONID = "ppay_sso_sessionid";
    /**
     * redirect url (web client)
     */
    public static final String REDIRECT_URL = "redirect_url";
    /**
     * sso user, request attribute (web client)
     */
    public static final String SSO_USER = "sso_user";
    /**
     * sso server address (web + token client)
     */
    public static final String SSO_SERVER = "sso_server";
    /**
     * login url, server relative path (web client)
     */
    public static final String SSO_LOGIN = "/login";
    /**
     * logout url, server relative path (web client)
     */
    public static final String SSO_LOGOUT = "/logout";
    /**
     * logout path, client relatice path
     */
    public static final String SSO_LOGOUT_PATH = "SSO_LOGOUT_PATH";
    /**
     * excluded paths, client relatice path, include path can be set by "filter-mapping"
     */
    public static final String SSO_EXCLUDED_PATHS = "SSO_EXCLUDED_PATHS";
    /**
     * APP token 认证
     */
    public static final String AUTH_TYPE_APP = "APP";
    /**
     * web cookie 认证
     */
    public static final String AUTH_TYPE_WEB = "WEB";

    /**
     * login fail result
     */
    public static final CommonResult<String> SSO_LOGIN_FAIL_RESULT = new CommonResult(501, "sso not login.");
}
