package cc.ppay.sso.client.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 客户端配置
 *
 * @author Ade.Xiao 2021/1/30 16:43
 */
@ConfigurationProperties(prefix = "ppay.sso.client", ignoreInvalidFields = true)
public class SsoClientProperties {
    /**
     * 认证中心地址
     */
    private String ssoServer = "http://127.0.0.1:8080/sso-server";
    /**
     * 退出地址
     */
    private String logoutPath = "/logout";
    /**
     * 不拦截地址
     */
    private String excludedPaths;
    /**
     * 客户端类型， token 或者 web
     */
    private String type;

    public String getSsoServer() {
        return ssoServer;
    }

    public void setSsoServer(String ssoServer) {
        this.ssoServer = ssoServer;
    }

    public String getLogoutPath() {
        return logoutPath;
    }

    public void setLogoutPath(String logoutPath) {
        this.logoutPath = logoutPath;
    }

    public String getExcludedPaths() {
        return excludedPaths;
    }

    public void setExcludedPaths(String excludedPaths) {
        this.excludedPaths = excludedPaths;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
