package cc.ppay.sso.client.config;

import cc.ppay.sso.client.filter.SsoWebFilter;
import cc.ppay.sso.client.service.impl.SsoClientServiceImpl;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * sso web 客户端配置
 *
 * @author Ade.Xiao 2021/1/29 21:03
 */
@Configuration
@ConditionalOnProperty(prefix = "ppay.sso.client", name = "type", havingValue = "web")
public class SsoWebClientConfig implements DisposableBean {
    @Autowired
    private SsoClientProperties ssoClientProperties;

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean registration = new FilterRegistrationBean();

        registration.setName("SsoWebFilter");
        registration.setOrder(1);
        registration.addUrlPatterns("/*");
        registration.setFilter(new SsoWebFilter(ssoClientProperties.getSsoServer(), ssoClientProperties.getLogoutPath(), ssoClientProperties.getExcludedPaths(), new SsoClientServiceImpl(ssoClientProperties.getSsoServer())));

        return registration;
    }

    @Override
    public void destroy() throws Exception {
    }

}
