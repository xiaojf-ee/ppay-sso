package cc.ppay.sso.client.config;

import cc.ppay.sso.client.filter.SsoTokenFilter;
import cc.ppay.sso.client.service.SsoClientService;
import cc.ppay.sso.client.service.impl.SsoClientServiceImpl;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * sso token 客户端拦截配置
 *
 * @author Ade.Xiao 2021/1/30 16:47
 */
@Configuration
@ConditionalOnProperty(prefix = "ppay.sso.client", name = "type", havingValue = "token")
public class SsoTokenClientConfig implements DisposableBean {
    @Autowired
    private SsoClientProperties ssoClientProperties;

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        SsoClientService ssoClientService = new SsoClientServiceImpl(ssoClientProperties.getSsoServer());


        FilterRegistrationBean registration = new FilterRegistrationBean();


        registration.setName("SsoWebFilter");
        registration.setOrder(1);
        registration.addUrlPatterns("/*");

        registration.setFilter(new SsoTokenFilter(ssoClientProperties.getSsoServer(), ssoClientProperties.getLogoutPath(), ssoClientProperties.getExcludedPaths(), ssoClientService));

        return registration;
    }

    @Override
    public void destroy() throws Exception {
    }

}
