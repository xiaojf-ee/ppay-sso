package cc.ppay.sso.client.service;

import cc.ppay.sso.common.model.SsoUser;

/**
 * 客户端业务接口
 *
 * @author Ade.Xiao 2021/1/30 14:32
 */
public interface SsoClientService {
    void logout(String sessionid);

    SsoUser loginCheck(String authTypeApp, String header);
}
