package cc.ppay.sso.client.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * client 配置
 *
 * @author Ade.Xiao 2021/1/30 16:40
 */
@Configuration
@EnableConfigurationProperties({SsoClientProperties.class})
public class SsoClientConfig {
}
