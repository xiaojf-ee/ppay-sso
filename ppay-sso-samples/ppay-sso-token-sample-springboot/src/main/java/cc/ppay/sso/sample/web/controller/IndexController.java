package cc.ppay.sso.sample.web.controller;

import cc.ppay.sso.common.constant.SsoConstant;
import cc.ppay.sso.common.model.CommonResult;
import cc.ppay.sso.common.model.SsoUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 首页
 *
 * @author Ade.Xiao 2021/1/29 15:14
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    @ResponseBody
    public CommonResult<SsoUser> index(HttpServletRequest request) {
        SsoUser ssoUser = (SsoUser) request.getAttribute(SsoConstant.SSO_USER);
        return new CommonResult(ssoUser);
    }

}