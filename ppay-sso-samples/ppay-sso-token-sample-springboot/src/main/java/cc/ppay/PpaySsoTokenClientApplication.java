package cc.ppay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 使用token令牌作为鉴权客户端
 *
 * @author Ade.Xiao 2021/1/29 15:15
 */
@SpringBootApplication
public class PpaySsoTokenClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(PpaySsoTokenClientApplication.class, args);
    }

}