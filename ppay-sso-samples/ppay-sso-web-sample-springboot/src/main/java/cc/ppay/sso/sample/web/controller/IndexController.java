package cc.ppay.sso.sample.web.controller;

import cc.ppay.sso.common.constant.SsoConstant;
import cc.ppay.sso.common.model.CommonResult;
import cc.ppay.sso.common.model.SsoUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author xuxueli 2017-08-01 21:39:47
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String index(Model model, HttpServletRequest request) {

        SsoUser ssoUser = (SsoUser) request.getAttribute(SsoConstant.SSO_USER);
        model.addAttribute("ssoUser", ssoUser);
        return "index";
    }

    @RequestMapping("/json")
    @ResponseBody
    public CommonResult<SsoUser> json(Model model, HttpServletRequest request) {
        SsoUser ssoUser = (SsoUser) request.getAttribute(SsoConstant.SSO_USER);
        return new CommonResult(ssoUser);
    }

}