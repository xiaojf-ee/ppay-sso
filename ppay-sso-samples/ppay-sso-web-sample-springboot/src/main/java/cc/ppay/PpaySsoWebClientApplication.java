package cc.ppay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 使用cookie作为鉴权客户端
 *
 * @author Ade.Xiao 2021/1/29 15:19
 */
@SpringBootApplication
public class PpaySsoWebClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(PpaySsoWebClientApplication.class, args);
    }

}
