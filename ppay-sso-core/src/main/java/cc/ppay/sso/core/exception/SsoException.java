package cc.ppay.sso.core.exception;

/**
 * sso 自定义异常
 *
 * @author Ade.Xiao 2021/1/29 16:21
 */
public class SsoException extends RuntimeException {
    private static final long serialVersionUID = -405068806622394357L;

    public SsoException(String msg) {
        super(msg);
    }

    public SsoException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public SsoException(Throwable cause) {
        super(cause);
    }
}
