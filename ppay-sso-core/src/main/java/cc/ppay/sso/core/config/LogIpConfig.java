package cc.ppay.sso.core.config;


import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 获取服务器ip地址
 *
 * @author Ade.Xiao 2021/1/29 18:10
 */
public class LogIpConfig extends ClassicConverter {
    private static final Logger log = LoggerFactory.getLogger(LogIpConfig.class);

    @Override
    public String convert(ILoggingEvent event) {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.error("获取日志Ip异常", e);
        }
        return null;
    }
}
