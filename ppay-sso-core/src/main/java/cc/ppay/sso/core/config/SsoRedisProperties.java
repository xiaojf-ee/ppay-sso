package cc.ppay.sso.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * sso redis 配置
 *
 * @author Ade.Xiao 2021/1/29 15:38
 */
@ConfigurationProperties(prefix = "ppay.sso.redis", ignoreInvalidFields = true)
public class SsoRedisProperties {
    /**
     * 缓存失效时间
     */
    private int redisExpireMinute = 1440;

    public int getRedisExpireMinute() {
        return redisExpireMinute;
    }

    public void setRedisExpireMinute(int redisExpireMinute) {
        this.redisExpireMinute = redisExpireMinute;
    }
}
