package cc.ppay.sso.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 属性配置
 *
 * @author Ade.Xiao 2021/1/29 15:23
 */
@ConfigurationProperties(prefix = "ppay.sso", ignoreInvalidFields = true)
public class SsoProperties {
    private String sessionidEncryKey = "@iwjefiaw_jofiu3";

    public String getSessionidEncryKey() {
        return sessionidEncryKey;
    }

    public void setSessionidEncryKey(String sessionidEncryKey) {
        this.sessionidEncryKey = sessionidEncryKey;
    }
}