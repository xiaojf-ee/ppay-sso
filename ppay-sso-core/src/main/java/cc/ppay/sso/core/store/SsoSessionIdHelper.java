package cc.ppay.sso.core.store;

import cc.ppay.sso.common.model.SsoUser;
import cc.ppay.sso.core.config.SsoProperties;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * sessionid 操作类
 * <p>
 * client: cookie = [userid#version]
 * server: redis
 * key = [userid]
 * value = user (user.version, valid this)
 * <p>
 * //   group         The same group shares the login status, Different groups will not interact
 *
 * @author Ade.Xiao 2021/1/29 17:08
 */
@Component
public class SsoSessionIdHelper {
    private static final Logger log = LoggerFactory.getLogger(SsoSessionIdHelper.class);

    @Autowired
    private SsoProperties ssoProperties;
    private AES aes = null;

    @PostConstruct
    public void init() {
        aes = SecureUtil.aes(ssoProperties.getSessionidEncryKey().getBytes());
    }

    /**
     * make client sessionId
     *
     * @param ssoUser
     * @return
     */
    public String makeSessionId(String type, String ip, SsoUser ssoUser) {
        String sessionId = type.concat("_").concat(ip).concat("_").concat(ssoUser.getUserid()).concat("_").concat(ssoUser.getVersion());
        String encryptBase64 = aes.encryptBase64(sessionId);
        return encryptBase64;
    }

    /**
     * 从sessionid 中解析获取后台存储cookie内容的key
     *
     * @param sessionId
     * @return
     * @author Ade.Xiao 2021/1/29 15:07
     */
    public String parseStoreKey(String t, String sessionId) {
        try {
            if (StrUtil.isBlank(sessionId)) {
                return null;
            }

            sessionId = aes.decryptStr(sessionId);

            if (sessionId != null && sessionId.indexOf("_") > -1) {
                String[] sessionIdArr = sessionId.split("_");
                if (sessionIdArr.length == 4 && sessionIdArr[0] != null && sessionIdArr[0].trim().length() > 0) {
                    String type = sessionIdArr[0].trim();
                    String userId = sessionIdArr[2].trim();

                    if (!type.equalsIgnoreCase(t)) {
                        return null;
                    }

                    return type.concat("_").concat(userId);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * parse version from sessionId
     *
     * @param sessionId
     * @return
     */
    public String parseVersion(String sessionId) {
        try {
            if (StrUtil.isBlank(sessionId)) {
                return null;
            }

            sessionId = aes.decryptStr(sessionId);

            if (sessionId != null && sessionId.indexOf("_") > -1) {
                String[] sessionIdArr = sessionId.split("_");
                if (sessionIdArr.length == 4 && sessionIdArr[1] != null && sessionIdArr[1].trim().length() > 0) {
                    String version = sessionIdArr[3].trim();
                    return version;
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

}
