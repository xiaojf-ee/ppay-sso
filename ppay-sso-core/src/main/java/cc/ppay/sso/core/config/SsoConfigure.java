package cc.ppay.sso.core.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * sso 配置
 *
 * @author Ade.Xiao 2021/1/29 15:22
 */
@Configuration
@EnableConfigurationProperties({SsoProperties.class, SsoRedisProperties.class})
public class SsoConfigure {
    
}