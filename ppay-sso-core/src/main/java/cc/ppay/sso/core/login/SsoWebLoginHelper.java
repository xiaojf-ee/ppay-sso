package cc.ppay.sso.core.login;

import cc.ppay.sso.common.constant.SsoConstant;
import cc.ppay.sso.common.model.SsoUser;
import cc.ppay.sso.common.util.CookieUtil;
import cc.ppay.sso.core.store.SsoLoginStore;
import cc.ppay.sso.core.store.SsoSessionIdHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录登出辅助类
 *
 * @author Ade.Xiao 2021/1/29 17:25
 */
@Component
public class SsoWebLoginHelper {
    @Autowired
    private SsoLoginStore ssoLoginStore;
    @Autowired
    private SsoTokenLoginHelper ssoTokenLoginHelper;
    @Autowired
    private SsoWebLoginHelper ssoWebLoginHelper;
    @Autowired
    private SsoSessionIdHelper ssoSessionIdHelper;

    /**
     * client login
     *
     * @param response
     * @param sessionId
     * @param ifRemember true: cookie not expire, false: expire when browser close （server cookie）
     * @param ssoUser
     * @author Ade.Xiao 2021/1/29 17:25
     */
    public void login(HttpServletResponse response,
                      String sessionId,
                      SsoUser ssoUser,
                      boolean ifRemember) {

        String storeKey = ssoSessionIdHelper.parseStoreKey(SsoConstant.AUTH_TYPE_WEB, sessionId);
        if (storeKey == null) {
            throw new RuntimeException("parseStoreKey Fail, sessionId:" + sessionId);
        }

        ssoLoginStore.put(storeKey, ssoUser);
        CookieUtil.set(response, SsoConstant.SSO_SESSIONID, sessionId, ifRemember);
    }

    /**
     * client logout
     *
     * @param request
     * @param response
     * @author Ade.Xiao 2021/1/29 17:25
     */
    public void logout(HttpServletRequest request,
                       HttpServletResponse response) {

        String cookieSessionId = CookieUtil.getValue(request, SsoConstant.SSO_SESSIONID);
        if (cookieSessionId == null) {
            return;
        }

        String storeKey = ssoSessionIdHelper.parseStoreKey(SsoConstant.AUTH_TYPE_WEB, cookieSessionId);
        if (storeKey != null) {
            ssoLoginStore.remove(storeKey);
        }

        CookieUtil.remove(request, response, SsoConstant.SSO_SESSIONID);
    }


    /**
     * login check
     *
     * @param request
     * @param response
     * @return
     * @author Ade.Xiao 2021/1/29 17:25
     */
    public SsoUser loginCheck(HttpServletRequest request, HttpServletResponse response) {

        String cookieSessionId = CookieUtil.getValue(request, SsoConstant.SSO_SESSIONID);

        // cookie user
        SsoUser ssoUser = ssoTokenLoginHelper.loginCheck(SsoConstant.AUTH_TYPE_WEB, cookieSessionId);
        if (ssoUser != null) {
            return ssoUser;
        }

        // redirect user

        // remove old cookie
        ssoWebLoginHelper.removeSessionIdByCookie(request, response);

        // set new cookie
        String paramSessionId = request.getParameter(SsoConstant.SSO_SESSIONID);
        ssoUser = ssoTokenLoginHelper.loginCheck(SsoConstant.AUTH_TYPE_WEB, paramSessionId);
        if (ssoUser != null) {
            CookieUtil.set(response, SsoConstant.SSO_SESSIONID, paramSessionId, false);    // expire when browser close （client cookie）
            return ssoUser;
        }

        return null;
    }


    /**
     * client logout, cookie only
     *
     * @param request
     * @param response
     * @author Ade.Xiao 2021/1/29 17:25
     */
    public void removeSessionIdByCookie(HttpServletRequest request, HttpServletResponse response) {
        CookieUtil.remove(request, response, SsoConstant.SSO_SESSIONID);
    }

    /**
     * get sessionid by cookie
     *
     * @param request
     * @return
     * @author Ade.Xiao 2021/1/29 17:25
     */
    public String getSessionIdByCookie(HttpServletRequest request) {
        String cookieSessionId = CookieUtil.getValue(request, SsoConstant.SSO_SESSIONID);
        return cookieSessionId;
    }


}
