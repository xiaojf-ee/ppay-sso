# ppay-sso

#### 介绍

基于xxl-sso进行重构，手撸了一个 统一认证中心， 支持分布式缓存、跨域、token、cookie、cookie加密等等功能， 接入方式简单、高效、安全， 告别oauth2 的臃肿和繁琐配置，有兴趣的给点个星星呀

#### 技术选型

| 名称                           | 版本          |
| ------------------------------ | ------------- |
| spring-boot                    | 2.2.6.RELEASE |
| spring-boot-starter-freemarker | 2.2.6.RELEASE |
| spring-boot-starter-data-redis | 2.2.6.RELEASE |
| spring-boot-starter-web        | 2.2.6.RELEASE |
| hutool                         | 4.6.4         |

#### 软件架构

![输入图片说明](https://images.gitee.com/uploads/images/2021/0130/231031_762d01b3_77658.png "屏幕截图.png")

#### 安装教程

- 启动sso-server

- 客户端

1. 配置属性，指定客户端认证方式和sso-server地址

```properties
### sso
ppay.sso.client.server=http://127.0.0.1:8080/sso-server
ppay.sso.client.type=web
```

2. 启动客户端

#### 使用说明

1. 访问业务系统
http://localhost:8084/


2. 首次未登陆，自动重定向到统一认证中心登录页
http://127.0.0.1:8080/sso-server/login?redirect_url=http://localhost:8084/

![输入图片说明](https://images.gitee.com/uploads/images/2021/0130/231111_9e6f8d9a_77658.png "屏幕截图.png")

3. 登陆完成后，携带ppay-sso-sessionid 自动重定向到业务系统
http://localhost:8084/ppay_sso_sessionid=Byt4AfTMUxdCUrHhuLGowWpHC3rLudBve9yMy/DcfGW3ResJz/pZ4nShO9QyqYkmM2Hv6cRkHyd5ynLPAbyzbg==

![输入图片说明](https://images.gitee.com/uploads/images/2021/0130/231154_6868bebc_77658.png "屏幕截图.png")

#### 相对xxl-sso 优化点

1. 去除jedis，修改为 spring-boot-starter-data-redis
2. 客户端取消直接访问redis，改为http请求sso-server获取认证信息
3. 最大程度利用springboot 自动属性配置，简化客户端接入复杂度
4. 重构代码，提高代码质量
5. 添加代码注释
6. 拆分工程，代码了利用率和冗余大幅度减少
7. 修复已知bug
8. 美化登录页，去掉无用静态代码，优化工程大小
9. cookie 加密、ip判断等增强措施
10. 支持token和web同时登录，更符合真实使用场景
11. 持续更新，后续将新增用户管理、日志管理、接入应用系统管理等